import React, { useState, useEffect } from "react";
import { Button } from "@mantine/core";
import DisplayRecipe from "./DisplayRecipe";

const axios = require("axios").default;

const GenerateBtn = (ingredientsString) => {
  const [recipeTitle, setRecipeTitle] = useState("");
  const [recipeImage, setRecipeImage] = useState("");
  const [recipeId, setRecipeId] = useState("");
  const [recipeInstructions, setRecipeInstructions] = useState("");

  const options = {
    method: "GET",
    url:
      "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients",
    params: {
      ingredients: ingredientsString,
      number: "1",
      ignorePantry: "false",
      ranking: "1",
    },
    headers: {
      "x-rapidapi-host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
      "x-rapidapi-key": process.env.REACT_APP_X_RapidAPI_Key,
    },
  };

  const URL_INSTRUCTIONS = {
    method: "GET",
    url:
      "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/" +
      recipeId +
      "/information",
    headers: {
      "X-RapidAPI-Host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
      "X-RapidAPI-Key": process.env.REACT_APP_X_RapidAPI_Key,
    },
  };

  const handleOnClick = () => {
    getRecipeData();
    getInstructionData();
  };

  let endpoints = [
    "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients",
    "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/" +
      recipeId +
      "/information",
  ];

  const getRecipeData = () => {
    axios
      .request(options)
      .then(function(response) {
        setRecipeId(response.data[0].id);
        setRecipeImage(response.data[0].image);
        setRecipeTitle(response.data[0].title);
        console.log(response.data);
        console.log({ recipeId });
      })
      .catch(function(error) {
        console.error(error);
      });
  };

  const getInstructionData = () => {
    axios
      .request(URL_INSTRUCTIONS)
      .then(function(response) {
        setRecipeInstructions(response.data.instructions);
        console.log(response.data.instructions);
      })
      .catch(function(error) {
        console.error(error);
      });
  };

  return (
    <div>
      <DisplayRecipe recipeTitle={recipeTitle} recipeImage={recipeImage} />
      <h4>{recipeInstructions}</h4>
      <Button onClick={handleOnClick}>Generate Recipe</Button>
    </div>
  );
};

export default GenerateBtn;
