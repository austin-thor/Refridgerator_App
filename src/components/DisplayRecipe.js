import React from "react";

const DisplayRecipe = ({ recipeTitle, recipeImage }) => {

  return (
    <div>
      <p>{recipeTitle}</p>
      <img src={recipeImage} alt="" />
    </div>
  );
};

export default DisplayRecipe;
