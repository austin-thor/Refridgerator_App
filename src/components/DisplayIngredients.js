import React from 'react';
import Ingredient from './Ingredient'

const DisplayIngredients = ({ ingredients, onDelete }) => {
  return (
    <div>
      {ingredients.map((ingredient) => (
        <Ingredient key={ingredient.id} ingredient={ingredient} onDelete={onDelete} />
      ))}
    </div>
  );
};

export default DisplayIngredients;
