import React from "react";
import Header from "./Header";
import { SignIn, auth } from "./Firebase";
import { useAuthState } from "react-firebase-hooks/auth";
import "./App.css";
import HomePage from "./HomePage";

function App() {
  const [user] = useAuthState(auth);

  return (
    <div className="App">
      <Header title={"In My Fridge"} />

      <section>{user ? <HomePage /> : <SignIn />}</section>
    </div>
  );
}

export default App;
