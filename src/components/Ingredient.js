import React from "react";
import { FaTimes } from "react-icons/fa";

const Ingredients = ({ ingredient, onDelete }) => {
  return (
    <div className="ingredient">
      <h3>
        {ingredient.name}{" "}
        <FaTimes
          style={{
            color: "red",
            cursor: "pointer",
            width: "20%",
          }}
          onClick={() => onDelete(ingredient.id)}
        />
      </h3>
    </div>
  );
};

export default Ingredients;
