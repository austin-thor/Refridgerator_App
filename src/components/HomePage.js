import React from "react";
import { useState, useEffect } from "react";
import DisplayIngredients from "./DisplayIngredients";
import { SignOut } from "./Firebase";
import GenerateBtn from "./GenerateBtn";
import { db } from "./Firebase";
import {
  collection,
  getDocs,
  addDoc,
  deleteDoc,
  doc,
} from "firebase/firestore";

export const ingredientCollectionsRef = collection(db, "ingredients");

const HomePage = () => {
  const [ingredients, setIngredients] = useState([]);
  const [newIngredient, setNewIngredient] = useState("");

  // Add Ingredient
  const addIngredient = async () => {
    await addDoc(ingredientCollectionsRef, { name: newIngredient });
    window.location.reload();
  };

  //UseEffect
  useEffect(() => {
    const getIngredient = async () => {
      const data = await getDocs(ingredientCollectionsRef);
      console.log(data);
      setIngredients(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };
    getIngredient();
  }, []);

  //Delete ingredient
  const deleteIngredient = async (id) => {
    const ingredientDoc = doc(db, "ingredients", id);
    await deleteDoc(ingredientDoc);
    window.location.reload();
  };

  let ingredientString = ingredients.map((food) => {
    return food["name"] + ", ";
  });

  return (
    <div>
      <div>
        <input
          type="text"
          placeholder="Enter Ingredients"
          onChange={(e) => setNewIngredient(e.target.value)}
        />
        <button type="button" onClick={addIngredient}>
          +
        </button>
      </div>
      {ingredients.length > 0 ? (
        <DisplayIngredients
          ingredients={ingredients}
          onDelete={deleteIngredient}
        />
      ) : (
        "Your Fridge is Empty!"
      )}

      <div>
        <div>
          <GenerateBtn ingredientString={ingredientString} />
        </div>
        <footer>
          <SignOut />
        </footer>
      </div>
    </div>
  );
};

export default HomePage;
