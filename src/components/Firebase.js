// Import the functions you need from the SDKs you need
import React from "react";
import "./App.css";
import { initializeApp } from "firebase/app";
import {
  getAuth,
  signInWithPopup,
  GoogleAuthProvider,
  signOut,
} from "firebase/auth";
import { Button } from "@mantine/core";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: "refridgerator-app.firebaseapp.com",
  projectId: "refridgerator-app",
  storageBucket: "refridgerator-app.appspot.com",
  messagingSenderId: "836927181589",
  appId: "1:836927181589:web:df05fd5c9c3d13be0fcf1c",
  measurementId: "G-XHM1E0ZXLH",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth();
export const db = getFirestore(app)

const provider = new GoogleAuthProvider();

export function SignIn() {
  const signInWithGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        const credentials = GoogleAuthProvider.credentialFromResult(result);
        const token = credentials.accessToken;

        const user = result.user;
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.messagingSenderId;
        const email = error.email;
        const credentials = GoogleAuthProvider.credentialFromError(error);
      });
  };

  return (
    <div className="signout-button">
      <Button
        variant="gradient"
        gradient={{ from: "teal", to: "blue", deg: 60 }}
        onClick={signInWithGoogle}
      >
        Sign In With Google
      </Button>
    </div>
  );
}

export function SignOut() {
  const signOutOfGoogle = () => {
    signOut(auth)
      .then(() => {
        console.log("successful logout");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <div className="signout-button">
      <Button
        variant="gradient"
        gradient={{ from: "teal", to: "blue", deg: 60 }}
        onClick={signOutOfGoogle}
      >
        Sign Out
      </Button>
    </div>
  );
}
