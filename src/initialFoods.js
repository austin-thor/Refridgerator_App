const initialFoods = [
  {
    id: 1,
    name: "panko bread crumbs",
  },
  {
    id: 2,
    name: "sweet chili sauce",
  },
  {
    id: 3,
    name: "chicken",
  },
  {
    id: 4,
    name: "chard",
  },
  {
    id: 5,
    name: "shitakes",
  },
  {
    id: 6,
    name: "lemon Peel",
  },
  {
    id: 7,
    name: "navy beans",
  },
  {
    id: 8,
    name: "eggplants",
  },
  {
    id: 9,
    name: "bananas",
  },
  {
    id: 10,
    name: "cider",
  },
];

export default initialFoods;
