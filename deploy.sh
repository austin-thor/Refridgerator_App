echo "Switching to Branch main"
git checkout main

echo "Building app...."
npm run build

echo "Deploying files to server...."
scp -r build/* austin@45.79.192.25:/var/www/45.79.192.25/

echo "Done!"